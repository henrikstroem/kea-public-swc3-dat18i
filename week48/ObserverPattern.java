import java.util.*;

abstract class Subject {
    private HashSet<Observer> observers = new HashSet<Observer>();

    public void attach(Observer observer) {
        this.observers.add(observer);
    }

    public void detach(Observer observer) {
        this.observers.remove(observer);
    }

    public void notification() {
        for(Observer observer:observers) {
            observer.update();
        }
    }
}

class ConcreteSubject extends Subject implements Runnable {
    private int number = 0;

    public void run() {
        while(true) {
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            this.number = new Random().nextInt(10);
            this.notification();
        }
    }

    public int getUpdate() {
        return this.number;
    }
}

abstract class Observer {
    // private Subject subject;

    // public Observer(Subject subject) {
    //     this.subject = subject;
    // }

    public void update() {
        System.out.println(this.toString() + " got notified.");
    }
}

class ConcreteObserver extends Observer {
    private ConcreteSubject subject;

    public ConcreteObserver(ConcreteSubject subject) {
        // super(subject);
        this.subject = subject;
    }

    public void update() {
        super.update();
        System.out.println("The new number is: " + this.subject.getUpdate());
    }
}

public class ObserverPattern {

    public static void main(String[] args) {
        ConcreteSubject sub = new ConcreteSubject();
        Thread subthread = new Thread(sub);
        subthread.start();

        ConcreteObserver con1 = new ConcreteObserver(sub);
        ConcreteObserver con2 = new ConcreteObserver(sub);
        ConcreteObserver con3 = new ConcreteObserver(sub);

        sub.attach(con1);
        sub.attach(con2);
        sub.attach(con3);
    }
}