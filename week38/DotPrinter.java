public class DotPrinter {
    public static void main(String[] args) {
        // Dotter d1 = new Dotter("*");
        // Dotter d2 = new Dotter(".");

        Dotter2 d1 = new Dotter2("*------------------------------------------*");
        Dotter2 d2 = new Dotter2(".");

        Thread t1 = new Thread(d1);
        Thread t2 = new Thread(d2);

        // t1.setPriority(Thread.MAX_PRIORITY);
        // t2.setPriority(Thread.MIN_PRIORITY);

        t1.start();
        t2.start();

        try {
            t1.join();
            t2.join();
        } catch(InterruptedException ie) {
            ie.printStackTrace();
        }

        System.out.println("Now I am done");
    }
}


class Dotter implements Runnable {
    String dot;

    public Dotter(String dot) {
        this.dot = dot;
    }

    public void run() {
        for(int i = 0; i < 10000; i++) {
            System.out.print(dot);
        }   
    }
}

class Dotter2 extends Thread {
    String dot;

    public Dotter2(String dot) {
        this.dot = dot;
    }

    public void run() {
        for(int i = 0; i < 10000; i++) {
            // try {
            //     Thread.sleep(10);
            // } catch(InterruptedException ie) {
            //     ie.printStackTrace();
            // }
            System.out.print(dot);
        }   
    }
}