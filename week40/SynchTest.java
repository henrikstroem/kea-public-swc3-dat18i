import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

class ControlTower {
    void permission(String message) {
        synchronized(this) {
            System.out.print("[[ " + message);
            try {
                Thread.sleep(0);
            } catch(InterruptedException ie) {}
            System.out.println(" ]]");
        }
    }
}

class Flight implements Runnable {
    String message;
    ControlTower ct;
    Thread t;

    public Flight(ControlTower ct, String message) {
        this.ct = ct;
        this.message = message;
        t = new Thread(this);
        // t.start();
    }

    public void run() {
        this.ct.permission(message);
    }
}

public class SynchTest {
    public static void main(String[] args) {
        ControlTower ct = new ControlTower();

        ThreadPoolExecutor exec = (ThreadPoolExecutor) Executors.newFixedThreadPool(4);
        for(int i = 0; i < 10; i++) {
            System.out.println(i);
            exec.execute(new Flight(ct, "Flight " + i));
        }
        exec.shutdown();

        // Flight f1 = new Flight(ct, "Flight 1");
        // Flight f2 = new Flight(ct, "Flight 2");
        // Flight f3 = new Flight(ct, "Flight 3");
        // Flight f4 = new Flight(ct, "Flight 4");
        // Flight f5 = new Flight(ct, "Flight 5");

        // System.out.println("Done!");

        // try {
        //     f1.t.join();
        //     f2.t.join();
        //     f3.t.join();
        //     f4.t.join();
        //     f5.t.join();
        // } catch(InterruptedException ie) {}
    }
}